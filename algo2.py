from utils import *

PREV_UNKNOWN_TRANSACTIONS = 3000
unknown_transactions = {}

def check(graph, a, b, c):
    if a not in unknown_transactions:
        unknown_transactions[a] = 0
    c0 = check_is_known(graph, a, b)
    if c0:
        return True
    if unknown_transactions[a] + c0 > 8000 and unknown_transactions[a] + c0 > 3 * PREV_UNKNOWN_TRANSACTIONS:
        return False
    unknown_transactions[a] += c
    return True
