import algo1
import algo2
import algo3
import algo4

graph = dict()

def is_potential_fraud(a, b, c):
    if not algo1.check(graph, a, b, c):
        return True
    if not algo2.check(graph, a, b, c):
        return True
    if not algo3.check(graph, a, b, c):
        return True
    # Not used, because for sample tests algo3 is similar to algo4
    # if not algo4.check(graph, a, b, c):
    #    return True
    return False


def add_edge(a, b, c):
    if a not in graph:
        graph[a] = {}
    if b not in graph[a]:
        graph[a][b] = 0
    graph[a][b] += c

if __name__ == "__main__":
    # Read previous transactions..
    while True:
        s = input()
        if s == '---':
            break
        a, b, c = s.split()
        c = int(c)
        add_edge(a, b, c)

    # Analyze current transactions
    while True:
        s = input()
        if s == 'stop':
            break
        a, b, c = s.split()
        c = int(c)
        result = is_potential_fraud(a, b , c)
        if result:
            print('fraud')
        else:
            print('ok')
