from utils import *

def check(graph, a, b, c):
    if c < 5000:
        return True
    c0 = check_is_known(graph, a, b, m=None)
    if c > 3 * c0:
        return False
    else:
        return True
