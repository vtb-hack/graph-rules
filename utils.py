M = 3000
def check_is_known(graph, a, b, m=M):
    if a not in graph:
        return False
    if b not in graph:
        return False
    to = 0
    back = 0
    if a in graph[b]:
        back = graph[b][a]
    if b in graph[a]:
        to = graph[a][b]
    sm = to + back
    if m is None:
        return sm
    return sm >= m
