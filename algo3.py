from utils import *

PREV_UNKNOWN_TRANSACTIONS = 5000

unknown_transactions = {}

def check(graph, a, b, c):
    c0 = check_is_known(graph, a, b, m=None)
    if c0:
        return True
    if b not in unknown_transactions:
        unknown_transactions[b] = 0

    if unknown_transactions[b] + c > 3 * PREV_UNKNOWN_TRANSACTIONS:
        return False
    unknown_transactions[b] += c
    return True
